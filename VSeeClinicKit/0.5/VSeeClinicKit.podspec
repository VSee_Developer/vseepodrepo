#
#  Be sure to run `pod spec lint VSeeClinicKit.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "VSeeClinicKit"
  s.version      = "0.5"
  s.summary      = "A short description of VSeeClinicKit. This will be updated later"
  s.description  = "VSeeClinicKit to build your online clinic"

  s.homepage     = "www.vsee.com"


  s.author       = { "Ken Tran" => "khoa@vsee.com" }
  s.license      = { :type => 'VSee Inc.', :file => 'LICENSE' }

  s.platform     = :ios
  s.ios.deployment_target = '9.0'

  s.source       = { :git => "https://bitbucket.org/VSee_Developer/vseeclinickit/raw/0fc21f57b17bdac2db680f7fbab29171ad3a154d/VSeeClinicKit.zip", :tag => s.version.to_s }
  s.ios.vendored_frameworks = "VSeeClinicKit.framework"

  s.frameworks = "VideoToolbox", "UIKit", "SystemConfiguration", "Security", "Foundation", "CoreMedia", "CoreGraphics", "CFNetwork", "AVFoundation", "AudioToolbox", "Accelerate"
  s.libraries = "z", "resolv", "icucore", "c++"

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.0' }

  s.dependency 'AFNetworking', '~> 3.0'
  s.dependency 'MagicalRecord', '2.2'
  s.dependency 'SDWebImage', '~> 4.0.0'
  s.dependency 'MBProgressHUD', '~> 1.0.0'
  s.dependency 'UICKeyChainStore', '~> 2.1.0'
  s.dependency 'FontAwesomeKit', '~> 2.2.0'
  s.dependency 'GBDeviceInfo', '~> 4.2.2'
  s.dependency 'Eureka', '~> 4.0.1'
  s.dependency 'MLPAutoCompleteTextField', '~> 1.5'
  s.dependency 'ImagePicker', '~> 3.0.0'
  s.dependency 'HCSStarRatingView', '~> 1.4.5'
  s.dependency 'Whisper', '~> 6.0.2'
  s.dependency 'FSCalendar', '~> 2.7.9'
  s.dependency 'Stripe', '~> 9.4.0'
  s.dependency 'TTTAttributedLabel', '~> 2.0.0'

end
